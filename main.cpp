#include <iostream>
#include <vector>

using namespace std;

vector<vector<string>> rubrica;

void write(string s, int minChar = 30) {
    s.resize(minChar, ' ');
    cout << s;
}

void writeln(string s, int minChar = 30) {
    write(s, minChar);
    cout << endl;
}

int getIndexOf(string firstName, string lastName ) {
    int l = rubrica.size();
    for (int i = 0; i < l; i++) {
        vector<string> r = rubrica[i];
        if (r[0] == firstName && r[1] == lastName) return i;
    }
    return -1;
}

vector<string> addContact(string firstName, string lastName, string phone ) {
    vector<string> contact = { firstName, lastName, phone };
    rubrica.push_back(contact);
    return contact;
}

void printContact(int index) {
    vector<string> contact = rubrica[index];
    write(contact[0]);
    write(contact[1]);
    writeln(contact[2]);
}

void printContact(string firstName, string lastName) {
    printContact(getIndexOf(firstName, lastName));
}

void removeContact(int index) {
    rubrica.erase(rubrica.begin() + index);
}

void removeContact(string firstName, string lastName) {
    removeContact(getIndexOf(firstName, lastName));
}

void printContacts(int skip = 0, int limit = 10 ) {
    int size = rubrica.size();
    int l = size > skip + limit ? limit : size - skip;
    for (int i = skip; i < l; i++) {
        printContact(i);
    }
    string curPage = to_string(skip / limit + 1);
    string totPages = to_string(size / limit);
    writeln("PAGINA " + curPage + '/' + totPages);
}

string firstNames[] = { "Giovanni", "Alberto", "Teresa", "Pietro", "Pino", "Luisa", "Sara", "Caterina", "Ugo", "Assunta" };
string lastNames[] = { "Cracco", "Rossi", "Verti", "Marroni", "Cannavacciuolo", "Quadrato", "Messi", "Gattuso", "Caruso", "Buondonno" };
void seed(int elements) {
    for (int e = 0; e < elements; e++) {
        string phone;
        for ( int i = 0; i < 10; i++) {
            phone +=  to_string(rand() %10);
        }
        addContact(firstNames[rand() %10], lastNames[rand() %10], phone);
    }
}

void mainScreen() {
    writeln("PROGRAMMA RUBRICA!");
    writeln("Comandi:");
    writeln("Q - Esci");
    writeln("H - Torna alla pagina principale");
    writeln("I - Inserisci contatto");
    writeln("D - Elimina contatto");
    writeln("L - Visualizza contatti");
}

void insertScreen() {

}

void deleteScreen() {

}

void listScreen() {
    printContacts(0, 10);
}

int main() {

    seed(100);
    mainScreen();
    while (true) {
        char input;
        cin >> input;

        switch (toupper(input)) {
            case 'Q':
                return 0;
            case 'I':
                insertScreen();
                break;
            case 'D':
                deleteScreen();
                break;
            case 'L':
                listScreen();
                break;
            case 'H':
                mainScreen();
                break;
            default:
                break;
        }
    }
}